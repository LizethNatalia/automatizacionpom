#Author: your.email@your.domain.com
#Keywords Summary :
#Feature: List of scenarios.
#Scenario: Business rule through list of steps with arguments.
#Given: Some precondition step
#When: Some key actions
#Then: To observe outcomes or validation
#And,But: To enumerate more Given,When,Then steps
#Scenario Outline: List of steps for data-driven as an Examples and <placeholder>
#Examples: Container for s table
#Background: List of steps run before each of the scenarios
#""" (Doc Strings)
#| (Data Tables)
#@ (Tags/Labels):To group Scenarios
#<> (placeholder)
#""
## (Comments)
#Sample Feature Definition Template
@Regresion
Feature: Verificar funcionalidad del form de popup Validation
El usuario debe poder ingresar al formulario los datos requeridos.
Cada campo del formulario realiza validaciones de obligatoriedad,
longitud, formato, el sistema debe presentar las validaciones respectivas
para cada campo a traves de un globo informatico.

 @CasoExitoso
Scenario: Diligenciamiento exitoso del formulario Popup Validation,
no se presenta ningun mensaje de validacion
Given Autentico en colorlib con usuario "demo" y clave "demo"
And Ingreso a la funcionalidad Forms Validation
When Diligencio Formulario Popup Validation
| Required | Select | MultipleS1 | Url | Email | Password1 |Password2 |MinSizes |MaxSize |Number |IP |Date |DateEarlier |
|Valor1|Golf|Tennis|http://www.valor1.com|valor@gmail.com|valor1|valor1|123456|345|-33.80|192.168.3.2|2021-12-01|2012/09/13|
Then Verifico ingreso exitoso

 @CasoAlterno
Scenario: Diligenciamiento con errores del formulario Popup Validation,
se presenta Globo informativo indicando error en el diligenciamiento de alguno de los campos
Given Autentico en colorlib con usuario "demo" y clave "demo"
And Ingreso a la funcionalidad Forms Validation
When Diligencio Formulario Popup Validation
| Required | Select | MultipleS1 | Url | Email | Password1 |Password2 |MinSizes |MaxSize |Number |IP |Date |DateEarlier |
| |Golf|Tennis|http://www.valor1.com|valor@gmail.com|valor1|valor1|123456|345|-33.80|192.168.3.2|2021-12-01|2012/09/13|
Then Verificar que se presente Globo Informativo de validacion


